﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stats : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (gameObject.name =="CoinTxt")
        {
            GetComponent<TextMesh>().text = "Coins: " + GM.coinTotal;
        }

        if (gameObject.name == "TimeTxt")
        {
            GetComponent<TextMesh>().text = "Time: " + GM.timeTotal;
        }

        if (gameObject.name == "StatusTxt")
        {
            GetComponent<TextMesh>().text = "Status: " + GM.lvlComStatus;
        }

    }
}
