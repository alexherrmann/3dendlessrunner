﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM : MonoBehaviour {

    public GameObject PlayerObj;

    public static float vertVel = 0;
    public static int coinTotal = 0;
    public static float timeTotal = 0;
    public static float zVelAdj = 1;
    public static string lvlComStatus = "";
    public float waittiload = 0;

    public Transform bbNormal;
    public Transform bbPitMid;

    public float bbZstart;
    private float bbZcount;

    public Transform CoinObj;
    public Transform PowerUpObj;
    public Transform ObstacelObj;

    public int randNum;

    // Use this for initialization
    void Start () {

        bbZcount = bbZstart;

        

        //Instantiate(bbNormal, new Vector3(0, 2.27f, bbZcount), bbNormal.rotation);
        //Instantiate(bbNormal, new Vector3(0, 2.27f, 39.525f), bbNormal.rotation);
        //Instantiate(bbPitMid, new Vector3(0, 2.27f, 43.525f), bbPitMid.rotation);
        //Instantiate(bbNormal, new Vector3(0, 2.27f, 47.525f), bbNormal.rotation);
        //Instantiate(bbNormal, new Vector3(0, 2.27f, 51.525f), bbNormal.rotation);


    }
	
	// Update is called once per frame
	void Update () {
        timeTotal += Time.deltaTime;

        if(lvlComStatus == "fail")
        {
            waittiload += Time.deltaTime;
        }

        if (waittiload > 2 )
        {
            SceneManager.LoadScene("comp");
        }

        if (bbZcount < 220)
        {
            randNum = Random.Range(1, 10);

            if (randNum < 2)
            {

                Instantiate(CoinObj, new Vector3(-1, 1, bbZcount), CoinObj.rotation);
            }

            if (randNum > 7)
            {

                Instantiate(PowerUpObj, new Vector3(1, 1, bbZcount), PowerUpObj.rotation);
            }

            if (randNum == 5)
            {

                Instantiate(ObstacelObj, new Vector3(0, 1, bbZcount), ObstacelObj.rotation);
            }


            Instantiate(bbNormal, new Vector3(0, 0, bbZcount), bbNormal.rotation);

            bbZcount += 4;
        }

    }


}
