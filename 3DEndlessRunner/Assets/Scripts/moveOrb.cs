﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moveOrb : MonoBehaviour {


    public float horizVel = 0;
    public int LaneNum = 2;
    public string controlLocked = "n";
    public GameObject DeadAnimationObj;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Rigidbody>().velocity = new Vector3(horizVel, GM.vertVel, 4);

        if (Input.GetKeyDown(KeyCode.A) && (LaneNum > 1) && (controlLocked == "n")) 
        {
            horizVel = -2;
            StartCoroutine(stopSlide());
            LaneNum -= 1;
            controlLocked = "y";
        }

        if (Input.GetKeyDown(KeyCode.D) && (LaneNum < 3) && (controlLocked == "n")) 
        {
            horizVel = 2;
            StartCoroutine(stopSlide());
            LaneNum += 1;
            controlLocked = "y";
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "lethal")
        {
            Destroy(gameObject);
            GM.zVelAdj = 0;
            Instantiate(DeadAnimationObj, transform.position, DeadAnimationObj.transform.rotation);
            GM.lvlComStatus = "fail";
        }

        if (other.gameObject.tag == "powerup")
        {
            Destroy(other.gameObject);
        }

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.name == "End")
        {
            SceneManager.LoadScene("comp");
            GM.lvlComStatus = "win";
   
        }

        if (other.gameObject.name == "Coin(Clone)")
        {
            Destroy(other.gameObject);
            GM.coinTotal += 1;
        
        }

    }

    IEnumerator stopSlide()
    {
        yield return new WaitForSeconds(.5f);
        horizVel = 0;
        controlLocked = "n";
    }

}
